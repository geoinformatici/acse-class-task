#include "axe_struct.h"
#include "axe_extended_struct.h"

/* -- function copied from `t_while_statement' -- */
t_forall_statement create_forall_statement()
{
   t_forall_statement statement;

   /* initialize the FORALL informations */
   statement.label_start = NULL;
   statement.label_continue = NULL;
   statement.label_break = NULL;

   /* return a new instance of `t_forall_statement' */
   return statement;
}

/* -- function copied from `t_while_statement' -- */
t_for_statement create_for_statement()
{
   t_for_statement statement;

   /* initialize the FOR informations */
   statement.label_condition = NULL;
   statement.label_end = NULL;
   statement.label_code = NULL;
   statement.label_assign = NULL;
   statement.location = REG_0;

   /* return a new instance of `t_for_statement' */
   return statement;
}
