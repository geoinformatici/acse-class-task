#define SIZE 32
#define FULL ((long int) 1<<SIZE)-1
#include "axe_struct.h"
#include "collections.h"
#include "axe_expressions.h"
#include "axe_utils.h"

t_axe_declaration *get_readonly_var(t_list *list, char *id) {
   t_axe_declaration *current;
   while (list) {
      current = (t_axe_declaration *) (list -> data);
      if (!strcmp(current -> ID, id)) return current;
      list = list -> next;
   }
   return NULL;
}

t_axe_expression custom_bin_numeric_op_mod(t_program_infos *program, t_axe_expression dividend, t_axe_expression divisor) {
   if ((dividend.expression_type == IMMEDIATE)  &&
       (divisor.expression_type == IMMEDIATE)) {
       return create_expression(dividend.value % divisor.value, IMMEDIATE);
   } else {
       t_axe_expression div, mul;
       div = handle_bin_numeric_op(program, dividend, divisor, DIV);
       mul = handle_bin_numeric_op(program, div, divisor, MUL);
       return handle_bin_numeric_op(program, dividend, mul, SUB);
   }
}

t_axe_expression custom_circular_lshift(t_program_infos *program, t_axe_expression number, t_axe_expression rotation) {
   if ((number.expression_type == IMMEDIATE)  && (rotation.expression_type == IMMEDIATE))
       return create_expression(number.value << rotation.value |
                                number.value >> (SIZE-rotation.value), IMMEDIATE);

   /* basic definitions */
   t_axe_expression exp1, exp2, mask, maskshift;
   t_axe_expression full = create_expression(FULL, IMMEDIATE);
   t_axe_expression one = create_expression(1, IMMEDIATE);
   t_axe_expression shift = create_expression(SIZE, IMMEDIATE);

   /* modulo operation */
   rotation = custom_bin_numeric_op_mod(program, rotation, shift);

   /* first shift */
   exp1 = handle_bin_numeric_op(program, number, rotation, SHL);

   /* second shift */
   shift = create_expression(SIZE, IMMEDIATE);
   shift = handle_bin_numeric_op(program, shift, rotation, SUB);
   exp2 = handle_bin_numeric_op(program, number, shift, SHR);

   /* mask to remove the bits when a right shift on a negative number happens */
   mask = create_expression(1 << (SIZE-1), IMMEDIATE);
   maskshift = handle_bin_numeric_op(program, shift, one, SUB);
   mask = handle_bin_numeric_op(program, mask, maskshift, SHR);
   mask = handle_bin_numeric_op(program, full, mask, SUB); /* XOR */

   /* apply the mask */
   exp2 = handle_bin_numeric_op(program, exp2, mask, ANDB);

   /* merge the two */
   return handle_bin_numeric_op(program, exp1, exp2, ORB);
}

t_axe_expression custom_circular_rshift(t_program_infos *program, t_axe_expression number, t_axe_expression rotation) {
   t_axe_expression size = create_expression(SIZE, IMMEDIATE);
   rotation = handle_bin_numeric_op(program, size, rotation, SUB);
   return custom_circular_lshift(program, number, rotation);
}
