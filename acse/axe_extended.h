#ifndef _AXE_EXTENDED_H
#define _AXE_EXTENDED_H
#define IMPLICIT "$implicit"

/* look for a variable in a `t_list' */
extern t_axe_declaration *get_readonly_var(t_list *list, char *id);

/* modulo operation */
extern t_axe_expression custom_bin_numeric_op_mod(t_program_infos *program, t_axe_expression dividend, t_axe_expression divisor);

/* circular shift */
extern t_axe_expression custom_circular_lshift(t_program_infos *program, t_axe_expression number, t_axe_expression rotation);
extern t_axe_expression custom_circular_rshift(t_program_infos *program, t_axe_expression number, t_axe_expression rotation);

#endif
