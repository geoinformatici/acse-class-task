#ifndef _AXE_EXTENDED_STRUCT_H
#define _AXE_EXTENDED_STRUCT_H

/* forall struct */
typedef struct t_forall_statement {
    t_axe_label *label_start;
    t_axe_label *label_continue;
    t_axe_label *label_break;
} t_forall_statement;

/* for struct */
typedef struct t_for_statement
{
   t_axe_label *label_condition;   /* this label points to the expression
                                    * that is used as loop condition */
   t_axe_label *label_end;         /* this label points to the instruction
                                    * that follows the while construct */
   t_axe_label *label_code;        /* this label point to the code block */
   t_axe_label *label_assign;      /* this label point to the assignments */
   int location;                   /* control where to jump */
} t_for_statement;

/* create an instance that will mantain infos about a forall statement */
extern t_forall_statement create_forall_statement();

/* create an instance that will mantain infos about a for statement */
extern t_for_statement create_for_statement();

#endif
